---
template: BlogPost
path: /de-magische-progressive-webapp
date: 2020-10-22T06:08:53.137Z
title: De magische progressive webapp
metaDescription: magische progressive webapp
thumbnail: /assets/dsc_0627.jpg
---
<!--StartFragment-->

# De magische Progressive Web App

Ben je een Angular developer? En heb je weleens een Angular applicatie gebouwd en die als mobiele app willen gebruiken? Dan heb je vást weleens gehoord van Progressive Web Apps (of in het kort: PWA). In deze TechTalk neem ik je mee in de wereld van PWA en hoe je in een paar simpele stappen een PWA van je website kan maken.



**Wat is Progressive Web App?**

De PWA is in principe een normale website met de functionaliteiten van een applicatie.Het grote verschil is dat een PWA werkt met een application shell. Dit noemen we ook wel app shell. Een app shell laadt namelijk alleen het hoognodige in om de PWA-interface te activeren, zoals HTML, CSS en JavaScript. Daarnaast heeft het nog meer voordelen. PWA biedt de mogelijkheid om offline gebruikt te worden, het laad onmiddellijk zonder problemen en neemt geen ruimte in beslag tijdens het installeren. Naar mijns inziens ideaal!

![](https://www.arcady.nl/media/1912/foto-1.png?width=451&height=333)

**Zes redenen waarom je voor PWA zou moeten kiezen:**

**1. Responsive.**

De PWA past zich per schermsoort aan, zoals resolutie, horizontaal of verticaal. Om dit te kunnen toepassen is het nodig om eerst je website mobile friendly te maken. Wanneer je website niet responsive is, zal je PWA dit ook niet zijn. Dus: Mobile first stylen.

**2. Secure.**

PWA maakt jouw omgeving veiliger. (Alleen toegang via HTTPS! (!)) Het maakt autorisatie en beveiligde transacties mogelijk, maar voorkomt ook ongeoorloofde toegang. Altijd handig.

**3. Offline.**

Heeft je gebruiker even geen internet of heel langzaam internet? Je PWA zal alsnog het één en ander inladen en laten zien. Ik vertel je hier later meer over.

**4. Updates.**

Heb je net een nieuwe versie van je website gereleased? De gebruiker kan, als je dit hebt toegevoegd, een melding krijgen dat er een nieuwe versie klaar staat. Met één druk op de knop refresht de pagina zich en krijgt je gebruiker de laatse versie van jouw website te zien.

**5. Voelt als een app.**

Het voelt en ziet eruit als een app. Zonder lange download- of installatietijd. Al overtuigd?

**6. Splashscreen.**

Nog niet overtuigd? Misschien helpt een splashscreen wel. Zoals je gewend bent hebben apps altijd een splashscreen. Nu jouw PWA ook!



**Genoeg gepraat, we gaan beginnen.**



**Stap 0: Gebruik Angular CLI.**

Als je al de officiële Angular CLI gebruikt zit je goed. Anders moet je dit nog even doen.

![](https://www.arcady.nl/media/1914/foto-2.png?width=500&height=124.66487935656836)



**Stap 1: Verkrijg alles wat je nodig hebt voor een PWA.**

Gebruik de volgende command om het broodnodige te installeren:

![](https://www.arcady.nl/media/1915/foto-3.png?width=500&height=36.29032258064516)

Wat voegt dit toe?

* Het zet je service workers op true.serviceWorker: true. ServiceWorker : true
* Het voegt de manifest.json toe in de assets
* In de index.html worden twee nieuwe lijnen toegevoegd:

  * <meta name=”theme-color”> tag
  * <link rel=”manifest”> tag die linkt met de manifest.json
* Het importeert***ServiceWorkerModule***in je app.
* Het voegt icons toe in de assets folder
* Het maakt twee bestanden aan: manifest.json en ngsw-config.json



**Stap 2: De manifest.json.**

Dit document wordt aangemaakt door de Angular CLI.

Het mooie aan een PWA is dat je een app-icoontje krijgt op het startscherm van je telefoon. Echter moet je daarvoor wel wat toevoegen in je manifest. Het minimale wat hij nodig heeft, is:

**“short_name”**of**“name”**,**“start_url”**,**“icons”**&**“display”**zijn vereist.

Dan zal je manifest file er ongeveer zo uitzien:

**Voorbeeld:**



Dit json-bestand regelt enkele dingen voor je. Zoals welk app-icon wordt gebruikt wanneer de gebruiker de website toevoegt aan het homescreen.

Daarnaast creëert de manifest een mooi splashscreen voor je en gebruikt hij de kleuren die je meegeeft. Zoals de***theme-color***(**Let op!**Wanneer je deze aanpast moet je ook de**value**in de**index.html**aanpassen hiervan.)

Met het start_url geef je aan waar je app moet openen.

Ga hier een beetje mee spelen. Probeer verschillende kleuren, icons of zelfs een ander beginscherm! Zo krijg je het steeds meer in de vingers.

De gebruiker krijgt als hij op jouw website is een pop-up aan de onderkant van zijn scherm om de webapp aan zijn of haar homescreen toe te voegen. Echter gaat dat niet zomaar. Hiervoor moet je o.a. een valide manifest file hebben, voldoen aan HTTPS, in het bezit zijn van een geldige geregistreerde service worker en moet de website minimaal twee keer bezocht worden in vijf minuten tijd.

Zo ziet je app-icon er bijvoorbeeld uit:

![](https://www.arcady.nl/media/1917/foto-5.png?width=266&height=176)



**Stap 3**:**Service Worker config file.**

Eerder vertelde ik al dat de install ook een ngsw-config.json aanmaakt in de root van je project. Hierin kun je aangeven of je bepaalde files wilt ‘lazy loaden’ of ‘prefetchen’. Dit gebeurt op het moment dat een gebruiker je PWA download. Als voorbeeld laat ik je mijn ngsw-config.json zien:

![](https://www.arcady.nl/media/1913/foto-6.png?width=500&height=437.2442019099591)

Zoals je ziet in mijn code wil ik graag dat in de app map de index.html, favicon, .css en .js geprefetch wordt bij het downloaden van de PWA. En dat de assets gebruik mogen maken van de lazy loading.

Voor meer informatie kun je hier eens naar kijken:<https://angular.io/guide/service-worker-config>



Na het uitvoeren van deze drie stappen is jouw website een PWA geworden. Uiteraard kan ik je hier nog veel meer vertellen. Daarom vertel ik je graag in een volgende blog wat je nog meer kan met de Service Workers en ga ik in op Google Lighthouse.



Mocht je vragen of opmerkingen hebben hierover bericht mij dan op LinkedIn of mail naar[jasper.ikink@arcady.nl](mailto:jasper.ikink@arcady.nl)



[<!--EndFragment-->](https://www.arcady.nl/updates/new-face-hasan/)
